"use strict";
var middleware = require("errorhandler");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:errorhandler");
  debug("start");

  if (process.env.NODE_ENV === 'development') {
    debug(".useError in development");
    imports.express.useError(middleware());
  }

  debug("register nothing");
  register(null, {});
};
